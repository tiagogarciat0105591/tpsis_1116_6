<?php

use App\Category;
use App\Product;
use Illuminate\Support\Facades\Input;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/


Route::get('/', function() {
    return redirect('/home');
});


Route::get('/home', [
  'as'=> 'home',
  'uses'=> 'HomeController@index']);

Route::get('/login', 'LoginController@index');

Route::get('categories/index', 'CategoriesController@index')->name('categories.index');
Route::get('categories/create', 'CategoriesController@create')->name('categories.create');
Route::post('categories/create', 'CategoriesController@store')->name('submit');
Route::get('categories/{id}/edit', 'CategoriesController@edit');
Route::put('categories/{id}', 'CategoriesController@update');
Route::delete('categories/{id}', 'CategoriesController@destroy');

Route::get('products/index', 'ProductsController@index')->name('products.index');
Route::get('products/create', 'ProductsController@create')->name('products.create');
Route::post('products/create', 'ProductsController@store')->name('submit');
Route::any('/search',function() {

    $products = DB::table('products')->paginate(10);

    $categories = Category::all();

    $products = Product::with('category')->get();

    $term = Input::get('q');


    $products = Product::whereHas('category', function($query) use($term) {
        $query->where('name', 'like', '%'.$term.'%');
    })->orWhere('name','LIKE','%'.$term.'%')->get();

    $products = Product::whereHas('localization', function($query) use($term) {
        $query->where('name', 'like', '%'.$term.'%');
    })->orWhere('name','LIKE','%'.$term.'%')->get();

    if(count($products) > 0)
    {
        return view('products.index', ['products'=>$products, 'categories'=>$categories])->withDetails($products)->withQuery ( $term );
    }
    else
    {
        return view('products.index', ['products'=>$products, 'categories'=>$categories])->with('errorMessageDuration', 'No Details found. Try to search again');
    }

});


Route::get('/carts', 'CartController@index')->name('listCart');
Route::post('/carts', 'CartController@store')->name('addToCart');
Route::delete('/carts/{id}', 'CartController@destroy')->name('destroyFromCart');

Route::get('/favorites', 'FavoritesController@listfavorites')->name('listFavorite');
Route::post('/favorites', 'FavoritesController@addfavorites')->name('addToFavorites');
Route::delete('/favorites/{id}', 'FavoritesController@destroyfavorites')->name('destroyFavorites');

Route::get('/checkout', 'CheckoutController@index')->name('checkout');

Route::Auth();