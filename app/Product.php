<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    protected $table ='products';

    protected $fillable = ['name', 'description', 'image', 'price', 'category_id'];

    public function category()
    {
        return $this->belongsTo('App\Category','category_id');
    }

    public  function favorites()
    {
     return $this->hasMany(\App\Favorite);
    }

    public function localization()
    {
        return $this->belongsTo( 'App\Localization', 'localization_id');
    }

}
