<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $table ='favorites';

    protected $fillable = ['product_id'];

    public function product()
    {
        return $this->belongsTo('App\Product','product_id');
    }
}
