<?php

namespace App\Http\Controllers;

use App\Favorite;
use Illuminate\Http\Request;
use App\Category;
use App\Product;
use DB;


class FavoritesController extends Controller
{
    public function addfavorites(Request $request)
    {
        $bool = 0;
        $allfavorites = Favorite::all();
        $favorite_product = $request->get('product_id');

        foreach($allfavorites as $allfavorite)
        {
            if($favorite_product == $allfavorite->product_id)
            {
                $bool = 1;
            }
        }

        if($bool == 0)
        {
            $favorite_product = new Favorite;
            $favorite_product->product_id = request()->get('product_id');

            $favorite_product->save();
            return redirect()->route('products.index')->with('success','Product favorited');
        }
        else
        {
            $favorite = Favorite::where( 'product_id', '=', $favorite_product);
            $favorite->increment('quantity');
        }

        return redirect()->route('products.index')->with('success','Product favorited');
    }

    public  function listfavorites()
    {
        $categories = Category::all();

        $favorite = Favorite::all();


        return view('favorites.index', ['favorites'=>$favorite, 'categories'=>$categories]);
    }

    public function destroyfavorites($id)
    {
        $favorite = Favorite::where( 'id', '=', $id)->get();
        if($favorite[0]['quantity'] > 1)
        {
            $favorite[0]->decrement('quantity');
            $categories = Category::all();
            $favorites = Favorite::all();
            return view('favorites.index', ['favorites'=>$favorites, 'categories'=>$categories])->with('success','Unliked');
        }
        else
        {
            $favorite[0]->delete();
            $categories = Category::all();
            $favorites = Favorite::all();
            return view('favorites.index', ['favorites'=>$favorites, 'categories'=>$categories])->with('success','Deleted');
        }
    }
}
