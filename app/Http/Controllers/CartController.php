<?php

namespace App\Http\Controllers;

use App\Cart;
use App\CartProduct;
use App\Product;
use Illuminate\Http\Request;
use Auth;
use App\Category;
use Illuminate\Support\Facades\DB;


class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();

        $cart = Cart::find(Auth::user()->id);
        if(!$cart) {
            $cart = new Cart();
        }
        $amount = 0;

        if(!$cart->products) {
            $cart->products = [];
        }
            foreach ($cart->products as $product)
                $amount += $product->price * $product->pivot->quantity;


        return view('cart.index', compact('cart', 'amount'), ['categories'=>$categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product =  Product::findOrFail($request->get('product_id'));
        $cart = Cart::where('user_id', '=', Auth::user()->id)->first();

        if(!$cart)
        {
            $cart = new Cart;
            $cart->user_id = Auth::user()->id;
            $cart->save();
        }

        if ($cart->products->contains('id', $product->id)) {
            $cart_product = CartProduct::where( [['cart_id', '=', $cart->id], ['product_id', '=', $product->id]]);
            $cart_product->increment('quantity');
        } else
        {
            $cart->products()->attach($product->id);
        }

        return redirect()->route('products.index')->with('success','Product added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cart = Cart::find(Auth::user()->id)->first();
        $cart->products()->detach(Product::find($id));

        return redirect('/carts')->with('success','Product deleted successfully');
    }
}
