<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Favorite;
use App\Product;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $categories = Category::all();
      $favorite = Favorite::all();


        return view('home', ['favorites'=>$favorite, 'categories'=>$categories]);
    }

}
