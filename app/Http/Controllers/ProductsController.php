<?php

namespace App\Http\Controllers;

use App\Localization;
use Illuminate\Http\Request;
use DB;
use App\Product;
use App\Category;
use File;
use Illuminate\Support\Facades\Input;
use App\Favorite;


class ProductsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = DB::table('products')->paginate(10);

        $categories = Category::all();

        $products = Product::with('category')->get();


        return view('products.index', ['products'=>$products, 'categories'=>$categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
        $categories = Category::all();
        $products = Product::all();
        $localizations = Localization::all();

        return view('products.create', ['products'=>$products, 'categories'=>$categories, 'localizations'=>$localizations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categories = Category::all();
        $localizations = Localization::all();

        $this->validate($request, [
            'name' => 'required|max:255|unique:products,name,',
            'description' => 'required|max:255',
            'price' => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'image' => 'mimes:jpeg,png|max:10240']);

       $product = new Product;

       $product->name = $request->input('name');
       $product->description = $request->input('description');
       $product->category_id = Input::get('categories');
       $product->localization_id = Input::get('localizations');
       $product->price = $request->input('price');
       $product->image =  time().'.'.$request->image->getClientOriginalExtension();
       $request->image->move(public_path('/img/'), $product->image);

       $product->save();

        return redirect()->route('products.index', ['categories'=>$categories, 'localizations'=>$localizations])
            ->with('success','Product created successfully');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
