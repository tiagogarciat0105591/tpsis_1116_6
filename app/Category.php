<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = ['name'];
    protected $guarded = ['id'];



    public function product()
    {
      return $this->hasMany('App\Product');
    }
    public function getRelatedProductsAttribute()
    {
      $resulte = $this->products->pluck('id')->toArray();
      foreach ($this->childs as $child){
        $result = array_merge($resulte, $child->related_products_id);
      }
      return $resulte;
    }
    public function getTotalProductsAttribute()
    {
      return Product::whereIn('id', $this->related_products_id)->count();
    }

    public function getIconPathAttribute()
    {
      if($this->icon !== '')
      {
        return url('/icon/' . $this->icon);
      }
      else
      {
        return 'Icon invalido.';
      }
    }
}
