@extends('layouts.master')
@section('title')
    Preco Virtual - SHOPPING CART
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Shopping Cart</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-warning" href="{{ route('products.index') }}">
                    <i class="fa fa-angle-left"></i> Back</a>
            </div>
        </div>
    </div>
    <table class="table kullaniciTablosu">
        <thead>
        <tr>
            <th>Image</th>
            <th>Product Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($cart->products as $product)
            <tr>
                <td><img src="{{ asset('/img/' . $product->image) }}" border="3" height="100" width="100"/></td>
                <td>{{$product->name}}</td>
                <td>{{$product->price}} $</td>
                <td>{{$product->pivot->quantity}}</td>
                <td>
                    <form action="/carts/{{$product->id}}" method="post" style="display: inline">
                        {{method_field('DELETE')}}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger" value="Delete" >
                </td>

                <td class="hidden-xs text-center"><strong>Subtotal  {{$product->price * $product->pivot->quantity}}$</strong></td>

            </tr>
        @endforeach
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td class="hidden-xs text-center"><strong>Total  {{$amount}}$</strong></td>
        </tbody>

        <td><a href="{{route('checkout')}}" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></a></td>
    </table>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    @if(!empty($errorMessageDuration))
        <div class="alert alert-danger"> {{ $errorMessageDuration }}</div>
    @endif
@endsection