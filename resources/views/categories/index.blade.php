@extends('layouts.master')
@section('title')
    Preco Virtual - SHOW CATEGORIES
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 ">
            <div class="pull-left">
                <h2>Categories</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('categories.create') }}"> Create New Category</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>Id</th>
            <th>Nome</th>
            <th width="280px">Action</th>
        </tr>

        @foreach($categories as $category)
            <tr>
                <td>{{$category->id }}</td>
                <td>{{$category->name}}</td>

                <td>
                    <a class="btn btn-info" href="">Show</a>
                    <a class="btn btn-primary" href="/categories/{{$category->id}}/edit">Edit</a>
                    <form action="/categories/{{$category->id}}" method="post" style="display: inline">
                        {{method_field('DELETE')}}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger" value="Delete" >
                    </form>
                </td>
            </tr>
        @endforeach

    </table>
    {{ $categories->links() }}

@endsection
