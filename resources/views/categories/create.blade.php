@extends('layouts.master')
@section('title')
	Preco Virtual - NEW CATEGORY
@endsection
@section('content')
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">Create New Category</div>

					<div class="panel-body">

	<form method="POST" action="{{url('categories/create')}}"  enctype="multipart/form-data" class="form-horizontal">
	<div class="form-group">
		{{ csrf_field() }}

		<label class="col-md-4 control-label" for="ProductDescription">Name</label>
		<div class="col-md-5">
			<input name="name" type="text" id="name" class="name">
			<button class="btn btn-primary" type="submit"> Add Now</button>

		</div>
	</div>
</div>
</div>
</div>
</div>
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
		</form>
@endsection
