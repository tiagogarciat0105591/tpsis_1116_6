@extends('layouts.master')
@section('title')
    Preco Virtual - EDIT CATEGORY
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Category</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('categories.index') }}"> Back</a>
            </div>
        </div>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="/categories/{{$category->id}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
        <div class="form-group">
            {{ csrf_field() }}
            {{method_field('PUT')}}

            <label class="col-md-4 control-label" for="ProductDescription">Name</label>
            <div class="col-md-5">
                <input name="name" type="text" id="name" class="name" value="{{$category->name}}">
                <button class="btn btn-primary" type="submit"> Edit Now</button>
            </div>
        </div>

@endsection
