@extends('layouts.master')
@section('title')
    Preco Virtual - SHOW PRODUCTS
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Products</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('products.create') }}"> Create New Product</a>
            </div>
        </div>
    </div>
    <form action="/search" method="POST" role="search">
        {{ csrf_field() }}
        <div class="input-group">
            <input type="text" class="form-control" name="q"
                   placeholder="Search "> <span class="input-group-btn">
             span class="input-group-btn">
                            <button class="btn btn-success" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"><span style="margin-left:10px;">Search</span></button>
                        </span>
            </span>
        </div>
        </div>
        </div>
    </form>
    <div class="container">
        @if(isset($details))
        <p> The Search results for your query <b> {{ $query }} </b> are :</p>
        <h2>Sample Products details</h2>
            <table class="table kullaniciTablosu">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Product Name</th>
                    <th>Product Discription</th>
                    <th>Price</th>
                    <th>Category</th>
                    <th>Localization</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td><img src="{{ asset('/img/' . $product->image) }}" border="3" height="100" width="100"/></td>
                        <td>{{$product->name}}</td>
                        <td>{{$product->description}}</td>
                        <td>{{$product->price}} $</td>
                        <td>{{ $product->category->name }}</td>
                        <td>{{ $product->localization->name }}</td>
                        <td>
                            <form action="/carts" method="post" style="display: inline">
                                {{ csrf_field() }}
                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                <input type="submit" class="btn btn-success"  value="Add Now" >
                            </form>
                        </td>
                        <td>
                            <form action="/favorites" method="post" style="display: inline">
                                {{ csrf_field() }}
                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                <input type="submit" class="btn btn-warning"  value="Add Favorites">
                            </form>

                        </td>
                    </tr>
                </tbody>
                @endforeach
            </table>
        @else

    <table class="table kullaniciTablosu">
        <thead>
        <tr>
            <th>Image</th>
            <th>Product Name</th>
            <th>Product Discription</th>
            <th>Price</th>
            <th>Category</th>
            <th>Localization</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
        <tr>
            <td><img src="{{ asset('/img/' . $product->image) }}" border="3" height="100" width="100"/></td>
            <td>{{$product->name}}</td>
            <td>{{$product->description}}</td>
            <td>{{$product->price}} $</td>
            <td>{{ $product->category->name }}</td>
            <td>{{ $product->localization->name }}</td>
            <td>
                <form action="/carts" method="post" style="display: inline">
                    {{ csrf_field() }}
                    <input type="hidden" name="product_id" value="{{$product->id}}">
                    <input type="submit" class="btn btn-success"  value="Add Now" >
                </form>
            </td>
            <td>
                <form action="/favorites" method="post" style="display: inline">
                {{ csrf_field() }}
                <input type="hidden" name="product_id" value="{{$product->id}}">
                <input type="submit" class="btn btn-warning"  value="Add Favorites">
                </form>

            </td>


        </tr>

        @endforeach
        </tbody>
    </table>
@endif
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
            @if(!empty($errorMessageDuration))
                <div class="alert alert-danger"> {{ $errorMessageDuration }}</div>
    @endif

@endsection