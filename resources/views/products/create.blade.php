@extends('layouts.master')
@section('title')
    Preco Virtual - NEW PRODUCT
@endsection
@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add New Category</div>

                <div class="panel-body">

                    <form method="POST" action="{{url('products/create')}}"  enctype="multipart/form-data" class="form-horizontal">
                        <div class="form-group">
                        {{ csrf_field() }}
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="product_name">PRODUCT NAME</label>
                                <div class="col-md-4">
                                    <input id="name" name="name" placeholder="PRODUCT NAME" class="form-control input-md" required="" type="text">

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="product_name_fr">PRODUCT DESCRIPTION </label>
                                <div class="col-md-4">
                                    <textarea class="form-control" id="description" name="description"></textarea>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" >PRODUCT CATEGORY</label>
                                <div class="col-md-4">
                                    <select id="categories" class="form-control" name="categories">
                                    @foreach($categories as $category)
                                    <option  type="hidden"  name="category_id"  value="{{$category->id}}" class="form-control">{{$category->name}}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" >Localization</label>
                                <div class="col-md-4">
                                    <select id="localizations" class="form-control" name="localizations">
                                        @foreach($localizations as $localization)
                                            <option  type="hidden"  name="localization_id"  value="{{$localization->id}}" class="form-control">{{$localization->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="product_name">PRODUCT PRICE</label>
                                <div class="col-md-4">
                                    <input id="price" name="price" placeholder="PRODUCT PRICE" class="form-control input-md" required="" type="text">

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="filebutton">PRODUCT IMAGE</label>
                                <div class="col-md-4">
                                    <input id="image" type="file" class="form-control" name="image" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" ></label>
                                <div class="col-md-4">
                                    <button  type="submit" class="btn btn-primary">Add Now</button>
                                </div>
                            </div>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </form>



@endsection