@extends('layouts.master')
@section('title')
    Preco Virtual - MY FAVORITES
@endsection
@section('content')
    <table class="table kullaniciTablosu">
        <thead style="text-align: center">
        <tr>
            <th>Image</th>
            <th>Product Name</th>
            <th>Product Discription</th>
            <th>Price</th>
            <th>Category</th>
            <th>Likes</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach( $favorites as $favorite )
            <tr>
                <td><img src="{{ asset('/img/' . $favorite->product->image) }}" border="3" height="100" width="100"/></td>
                <td>{{$favorite->product->name}}</td>
                <td>{{$favorite->product->description}}</td>
                <td>{{$favorite->product->price}} $</td>
                <td>{{ $favorite->product->category->name }}</td>
                <td>{{$favorite->quantity}}</td>
                <td>
                    <form action="/favorites/{{$favorite->id}}" method="post" style="display: inline">
                        {{method_field('DELETE')}}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger" value="Delete" >
                    </form>
                </td>
            </tr>
        </tbody>
        @endforeach
    </table>

@endsection