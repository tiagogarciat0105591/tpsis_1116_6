
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Preço Virtual</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ URL::route('home') }}"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>


                        @guest
                            <li><a href="{{ route('login') }}"> <i class="fa fa-lock" aria-hidden="true"></i> Login</a></li>
                            <li><a href="{{ route('register') }}"> <i class="fa fa-sign-in" aria-hidden="true"></i> Register</a></li>
                            @else
                        <li><a href="{{route('addToCart')}}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Shopping Cart</a></li>
                        <li><a href="{{route('listFavorite')}}"><i class="fa fa-star" aria-hidden="true"></i> My Favorites</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-book" aria-hidden="true"></i> Categories<span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        @foreach($categories as $category)
                                            <li><a>{{$category->name}}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                                <li><a href="{{ route('products.index') }}"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Products</a></li>
                                @auth

                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i> Options<span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ route('products.create') }}">New Product</a></li>
                                        @if(Auth::User()->role === 'Admin')
                                        <li><a href="{{ route('categories.create') }}">New Category</a></li>
                                        <li><a href="{{ route('categories.index') }}">Show Categories</a></li>
                                        @else

                                        @endif
                                    </ul>
                                </li>

                                @endauth

                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                                @endguest
                    </ul>
        </div>
    </div>
</nav>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>