@extends('layouts.master')
@section('title')
    Preco Virtual - Home
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="https://www.denso.com/global/en/careers/life-at-denso/begin-with-people/0823-Begin_with_People-Edit2.jpg" alt="First slide">
                            <div class="carousel-caption">
                                <h3 style="text-shadow: black;">
                                    We are Creative</h3>

                            </div>
                        </div>
                        <div class="item">
                            <img src="http://www.sonata-software.com/tech-infra/sites/all/themes/sonata/images/slides/08.png" alt="Second slide">
                            <div class="carousel-caption">
                                <h3 style="text-shadow: black;">
                                    We are Dynamic</h3>
                            </div>
                        </div>
                        <div class="item">
                            <img src="https://www.zurich.co.uk/-/media/images/article-images/featured-article-banner/for_love_nor_money_1600x450.jpg?h=450&la=en&w=1600" alt="Third slide">
                            <div class="carousel-caption">
                                <h3 style="text-shadow: black;">
                                    Best Prices </h3>

                            </div>
                        </div>
                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span></a><a class="right carousel-control"
                                                                                     href="#carousel-example-generic" data-slide="next"><span class="glyphicon glyphicon-chevron-right">
                        </span></a>
                </div>
            </div>
        </div>
    </div>
    <div id="push">
    </div>
    <br>
    <br>
    </div>
    <div class="container" >
        <div class="row" >
            <div class="col-md-12">
                <div class="col-md-12">
                    <h2>Featured Products</h2>
                    <br>
                    @foreach( $favorites as $favorite )
                    <div class="thumbnail col-md-4 " >
                        <h4 class="text-center"><span class="label label-info">{{ $favorite->product->category->name }}</span></h4>
                        <img src="{{ asset('/img/' . $favorite->product->image) }}" border="3" style="width:50%;height:200px;"/>
                        <div class="caption">
                            <div class="row">
                                <div class="col-md-8">
                                    <h3 style="height: 75px;">{{$favorite->product->name}}</h3>
                                </div>
                                <div class="col-md-4 price">
                                    <h3>
                                        <label>{{$favorite->product->price}} $</label></h3>
                                </div>
                            </div>
                            <p>{{$favorite->product->description}}</p>
                            <div class="row">
                                @guest
                                <div class="col-md-3">
                                    <a href="{{ route('login') }}" class="btn btn-success btn-product">
                                        <span class="glyphicon glyphicon-shopping-cart"></span> Buy</a></div>
                                @endguest
                                @auth
                                        <form action="/carts" method="post" style="display: inline">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="product_id" value="{{$favorite->product->id}}">
                                            <input type="submit" class="btn btn-success"  value="Add Now" >
                                        </form>
                                    @endauth
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
