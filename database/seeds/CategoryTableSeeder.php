<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([

            'name' => 'Antiques'
            ]);

        DB::table('categories')->insert([

            'name' => 'Art'
        ]);

        DB::table('categories')->insert([

            'name' => 'Baby'

        ]);

        DB::table('categories')->insert([

            'name' => 'Books'
        ]);

        DB::table('categories')->insert([

            'name' => 'Business and Industrial'
        ]);

        DB::table('categories')->insert([

            'name' => 'Cameras and Photos'
        ]);


        DB::table('categories')->insert([

            'name' => 'Cell Phones and Accessories'
        ]);


        DB::table('categories')->insert([

            'name' => 'Clothing Shoes and Accessories'

        ]);


        DB::table('categories')->insert([

            'name' => 'Coins and Paper Money'
        ]);


        DB::table('categories')->insert([

            'name' => 'Collectibles'
        ]);


        DB::table('categories')->insert([

            'name' => 'Computers Tables and Networking'

        ]);


        DB::table('categories')->insert([

            'name' => 'Consumer Electronics'

        ]);


        DB::table('categories')->insert([

            'name' => 'Crafts'
        ]);


        DB::table('categories')->insert([

            'name' => 'Dolls and Bear '
        ]);


        DB::table('categories')->insert([

            'name' => 'DVDs and Movies'

        ]);


        DB::table('categories')->insert([

            'name' => 'Entertainment Memorabilia'

        ]);


        DB::table('categories')->insert([

            'name' => 'Gift Cards and Coupons'
        ]);


        DB::table('categories')->insert([

            'name' => 'Health and Beauty'
        ]);


        DB::table('categories')->insert([

            'name' => 'Home and Garden'
        ]);


        DB::table('categories')->insert([

            'name' => 'Jewelry and Watches'
        ]);


        DB::table('categories')->insert([

            'name' => 'Music'
        ]);


        DB::table('categories')->insert([

            'name' => 'Musical Instruments and Gear'
        ]);


        DB::table('categories')->insert([

            'name' => 'Pet Supplies'
        ]);


        DB::table('categories')->insert([
            'name' => 'Antiques'

        ]);


        DB::table('categories')->insert([

            'name' => 'Pottery and Glass'
        ]);


        DB::table('categories')->insert([

            'name' => 'Real Estate'
        ]);


        DB::table('categories')->insert([

            'name' => 'Specialty Services'
        ]);


        DB::table('categories')->insert([

            'name' => 'Sporting Goods'
        ]);


        DB::table('categories')->insert([

            'name' => 'Sports Man Carts and Fan Shop'
        ]);


        DB::table('categories')->insert([

            'name' => 'Stamps'
        ]);


        DB::table('categories')->insert([

            'name' => 'Tickets and Experiences'
        ]);


        DB::table('categories')->insert([

            'name' => 'Toys and Hobbies'
        ]);


        DB::table('categories')->insert([

            'name' => 'Travel'
        ]);


        DB::table('categories')->insert([

            'name' => 'Video Games and Consoles'
        ]);



    }
}
