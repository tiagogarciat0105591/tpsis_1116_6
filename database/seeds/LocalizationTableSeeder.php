<?php

use Illuminate\Database\Seeder;

class LocalizationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('localizations')->insert([

            'name' => 'Sao Joao da Madeira'
        ]);

        DB::table('localizations')->insert([

            'name' => 'Porto'
        ]);

    }
}
