<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Goncalo',
            'phone' => '911078378',
            'email' => 'sousa@gmail.com',
            'password'=> Hash::make('azores123'),
            'role' => 'Admin'
        ]);
    }
}
